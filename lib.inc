section .data
newline: db 0xA

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:  ; код возврата в rdi, где и должен быть
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
	pop rsi
    mov	rdx, rax
    mov	rdi, 1
    mov	rax, 1
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push 0
    push rdi
	mov rdi, rsp
	call print_string
	pop rdi
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, newline
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
    push rdx
    mov r9, rsp
    mov rax, rdi
    mov r10, 10
    dec rsp
    mov byte [rsp], 0
    .loop:
        xor rdx, rdx
        div r10
        or rdx, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    call print_string
    mov rsp, r9
    pop rdx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .pos
    .neg:
        push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
    .pos:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    .loop:
        mov cl, byte[rdi + r8]
        mov dl, byte[rsi + r8]
        cmp cl, dl
        jne .error
        inc r8
        test cl, cl
        jnz .loop
    inc rax
    .error:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	call string_length
	cmp rax, rdx
	jae .error
	xor rax, rax

    .loop:
        mov cl, byte[rdi + rax]
        mov [rsi + rax], cl
        inc rax
        test cl, cl
        jnz .loop
    ret

    .error:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
    xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14

    mov r12, rdi
    xor r13, r13
    mov r14, rsi

    .skip_loop:
        call read_char
        test rax, rax
        jz .success
        cmp rax, 0x9
        je .skip_loop
        cmp rax, 0xA
        je .skip_loop
        cmp rax, 0x20
        je .skip_loop

    .read_loop:
        dec r14
        test r14, r14
        jz .error
        mov byte[r12 + r13], al
        inc r13

        call read_char
        test rax, rax
        jz .success
        cmp rax, 0x9
        je .success
        cmp rax, 0xA
        je .success
        cmp rax, 0x20
        je .success

        jmp .read_loop

    .success:
        mov byte[r12 + r13], 0
        mov rax, r12
        mov rdx, r13
        jmp .end

    .error:
        xor rax, rax

    .end:
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    mov r10, 10

    .loop:
        mov r8b, byte[rdi + rdx]
        cmp r8b, '9'
        jg .end
        sub r8b, '0'
        jl .end

        imul rax, r10
        add rax, r8
        inc rdx
        jmp .loop

    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    ret

    .neg:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret
